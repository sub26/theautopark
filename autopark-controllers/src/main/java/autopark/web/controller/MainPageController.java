package autopark.web.controller;

import autopark.service.ICarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainPageController {

    @Autowired
    private ICarService carService;

    @RequestMapping(value = "/")
    public String handle(ModelMap modelMap) {
        modelMap.put("cars",carService.getCars());
        return "main.jsp";
    }
}



