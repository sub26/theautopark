package autopark.service;

import autopark.domain.Car;

import java.util.List;

/**
 * Created by  01 on 15.02.2016.
 */
public interface ICarService {
    List<Car> getCars();
}
