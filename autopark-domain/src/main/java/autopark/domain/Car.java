package autopark.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

/**
 * Created by  01 on 21.12.2015.
 */
@Entity
@Table(name = "ap_car")
public class Car extends Root {
    private CarVendorEnum vendor;
    private String model;
    private CarTypeEnum type;
    private CarLoadingCapacityEnum capacity;
    private Integer year;
    private String description;

    @Enumerated(EnumType.STRING)
    public CarVendorEnum getVendor() {
        return vendor;
    }

    public void setVendor(CarVendorEnum vendor) {
        this.vendor = vendor;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Enumerated(EnumType.STRING)
    public CarTypeEnum getType() {
        return type;
    }

    public void setType(CarTypeEnum type) {
        this.type = type;
    }

    @Enumerated(EnumType.STRING)
    public CarLoadingCapacityEnum getCapacity() {
        return capacity;
    }

    public void setCapacity(CarLoadingCapacityEnum capacity) {
        this.capacity = capacity;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
