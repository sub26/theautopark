package autopark.domain;

/**
 * Created by  01 on 15.02.2016.
 */
public enum CarVendorEnum {
    VAZ,
    HONDA,
    BMW,
    MERCEDES,
    VOLKSWAGEN,
    TOYOTA,
    VOLVO,
    SUZUKI,
    FORD,
    FERRARY,
    ZAPOROZEC,
    SCANIA,
    RENAULT,
    MAZ,
    KAMAZ,
    DAF,
    MAN
}
